<?php
date_default_timezone_set("Asia/Ho_Chi_Minh");

function isValid($date, $format = 'd/m/Y'){
    $dt = DateTime::createFromFormat($format, $date);
    return $dt && $dt->format($format) === $date;
}


$gender = array(0 => 'Nam', 1 => 'Nữ');
$faculties = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
$nameErr = $genderErr = $facultyErr = $birthErr = $fileErr = "<label></label><br>";


// Check exsits folder
$dir = "upload";
if ( !is_dir( $dir ) ) {
    mkdir( $dir );   
}


if (isset($_POST['submit'])) {
    session_start();
    $_SESSION["name"] = "";
    $_SESSION["gender"] = "";
    $_SESSION["faculty"] = $faculties[$_POST["faculty"]];
    $_SESSION["birth"] = "";
    $_SESSION["address"] = $_POST["address"];


    $check_upload = 1;
   

    if (!empty($_FILES["file_upload"]["tmp_name"])) {

        $date = date("Ymdhis");
        $target_dir = $dir . '/';
        $target_file = $target_dir . basename($_FILES["file_upload"]["name"]);
    
        $image_file_type = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        $origin_file_name = pathinfo($target_file, PATHINFO_FILENAME);
        
        // Rename upload file to {origin name}_YmdHis.{extension}
        $new_file_name = $origin_file_name . '_' . $date . '.' . $image_file_type;
        $target_file = $target_dir . $new_file_name;
        

        // Check file info
        $check_type = mime_content_type($_FILES["file_upload"]["tmp_name"]);
        $check_size = getimagesize($_FILES["file_upload"]["tmp_name"]);


        // Check if the uploaded file is an image or not
        if (substr($check_type, 0, 5) !== "image") {
            $fileErr = "<label class='error'>Tệp đã chọn không phải tệp hình ảnh.</label><br>";
            $check_upload = 0;
        } else {
            // Allow certain file formats
            if ($image_file_type != "jpg" && $image_file_type != "png" && $image_file_type != "jpeg") {
                $fileErr = "<label class='error'>Chỉ được tải lên tệp jpg, jpeg và png.</label><br>";
                $check_upload = 0;
            } else {
                // Check actual jpg, jpeg, png image or fake extension to upload
                if ($check_size === false) {
                    $fileErr = "<label class='error'>Tệp đã chọn không phải tệp hình ảnh.</label><br>";
                    $check_upload = 0;
                }
            }
        }

        if ($check_upload == 1)    
            move_uploaded_file($_FILES["file_upload"]["tmp_name"], $target_file);
    }

    $_SESSION["file_upload"] = $target_file;


    if (empty($_POST["name"]))
        $nameErr = "<label class='error'>Hãy nhập tên.</label><br>";
    else
        $_SESSION["name"] = $_POST["name"];

    if (empty($_POST["gender"]))
        $genderErr = "<label class='error'>Hãy chọn giới tính.</label><br>";
    else
        $_SESSION["gender"] =  $_POST["gender"];
    
    if (empty($_POST["birth"]))
        $birthErr = "<label class='error'>Hãy nhập ngày sinh.</label><br>";
    else if (!isValid($_POST["birth"]))
        $birthErr = "<label class='error'>Hãy nhập ngày sinh đúng định dạng.</label><br>";
    else
        $_SESSION["birth"] =  $_POST["birth"];

    if ($_SESSION["faculty"] === "")
        $facultyErr = "<label class='error'>Hãy chọn phân khoa.</label><br>";
    else if ($_SESSION["name"] != "" && $_SESSION["gender"] != "" && $_SESSION["birth"] != "" && $check_upload != 0) {
        header('Location: result_register.php');
    }
}
?>

<html>
    
    <head>
        <meta charset='UTF-8'>
        <link rel='stylesheet' href='form_register.css'>
    </head>

    <body>
        <fieldset>
            <form method='post' action='form_register.php' enctype="multipart/form-data">
            <?php
                echo $nameErr;
                echo $genderErr;
                echo $facultyErr;
                echo $birthErr;
                echo $fileErr;
            ?>

                <table>

                    <tr>
                        <td class='td'><label>Họ và tên<span class="star"> * </span></label></td>
                        <td>
                            <?php 
                            echo "<input type='text' id='input' class='box' name='name' value='";
                            echo isset($_POST['name']) ? $_POST['name'] : '';
                            echo "'>"; ?>
                        </td>
                            
                    </tr>

                    <tr>
                        <td class='td'><label>Giới tính<span class="star"> * </span></label></td>
                        <td>
                        <?php
                            for ($i = 0; $i < count($gender); $i++) {
                                echo
                                    "<input type='radio' name='gender' class='gender' value='" . $gender[$i] . "'";
                                echo (isset($_POST['gender']) && $_POST['gender'] == $gender[$i]) ? " checked " : "";
                                echo "/>" . $gender[$i];
                            }
                        ?>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class='td'><label>Phân khoa<span class="star"> * </span></label></td>
                        <td>
                            <select class='box' name='faculty'>
                            <?php
                                foreach ($faculties as $key => $value) {
                                    echo "<option";
                                    echo (isset($_POST['faculty']) && $_POST['faculty'] == $key) ? " selected " : "";
                                    echo " value='" . $key . "'>" . $value . "</option>";
                                }
                            ?>  
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td class='td'><label>Ngày sinh<span class="star"> * </span></label></td>
                        <td>
                            <?php 
                                echo "<input type='text' class='box' name='birth' placeholder='dd/mm/yyyy' onfocus='(this.type='date')' value='";
                                echo isset($_POST['birth']) ? $_POST['birth'] : '';
                                echo "'>"; ?>
                        </td>
                    </tr>

                    <tr>
                        <td class='td'><label>Địa chỉ</label></td>
                        <td>
                            <?php 
                                
                                echo "<input type='text' id='input' class='box' name='address' value='";
                                echo isset($_POST['address']) ? $_POST['address'] : '';
                                echo "'>";
                            ?>
                                
                        </td>
                    </tr>

                    <tr>
                        <td class='td'><label>Hình ảnh</label></td>
                        <td>
                            <?php 
                                
                                echo "<input type='file' id='input' name='file_upload' ";
                            
                            ?>
                                
                        </td>
                    </tr>

                </table>

                <button name='submit' type='submit'>Đăng ký</button>
                
            </form>

        </fieldset>

    </body>

</html>
