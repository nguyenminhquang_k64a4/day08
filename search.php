<?php
$faculties = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");


if (isset($_POST['search'])) {
    session_start();
    $_SESSION["faculty"] = $faculties[$_POST["faculty"]];
    $key_word = $_POST["key_word"];
}
?>

<html>
    
    <head>
        <meta charset='UTF-8'>
        <link rel='stylesheet' href='search.css'>
        <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

        <script>
			$(document).ready(function(){
				$('#btn_del').click(function(){				
                /*Clear all input type="text" box*/
                $('#form input[type="text"]').val('');
                /*Clear textarea using id */
                $('#form #input_falcuty').val('');
										
				});
			});

            // $(document).ready(function(){
			// 	$('#btn_search').click(function(){				
            //     $('#form #input_key_word').val($_SESSION["key_word"]);
										
			// 	});
			// });
		</script>

    </head>

    <body>
        <form id=form method='post' action='search.php' enctype='multipart/form-data'>
        <fieldset>
            <table>

                <tr>
                    <td class='td'><label>Khoa</label></td>
                    <td>
                        <select id = "input_falcuty" class='box' name='faculty'>
                        <?php
                            foreach ($faculties as $key => $value) {
                                echo "<option";
                                echo (isset($_POST['faculty']) && $_POST['faculty'] == $key) ? " selected " : "";
                                echo " value='" . $key . "'>" . $value . "</option>";
                            }
                        ?>  
                        </select>
                    </td>
                </tr>

                <tr>
                    <td class='td'><label>Từ khóa</label></td> 
                    <td>
                        <input type='text' id='input_key_word' class='box' name='key_word' value="<?php echo @$key_word ?>">
                    </td>
                </tr>          

            </table>

            <nobr><button name='delete' type='button' id="btn_del">Xóa</button><nobr>
  
            <button name='search' type='submit' id = "btn_search">Tìm kiếm</button>

            <table>
                <th>
                    <p>Số sinh viên tìm thấy: xxx</p>
                </th>

                <th>
                    <td>
                        <button class=btn_add name='add' type='button' onclick="location.href='form_register.php'">Thêm</button>
                    </td>
                </th>                
            </table>

            <div class='list_student'>
            <table>
                <colgroup>
                    <col span="1" style="width: 5%;">
                    <col span="1" style="width: 28%;">
                    <col span="1" style="width: 37%;">
                    <col span="1" style="width: 30%;">
                </colgroup>
                <tr>
                    <th>No</th>
                    <th>Tên sinh viên</th>
                    <th>Khoa</th>
                    <th>Action</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Nguyễn Văn A</td>
                    <td>Khoa học máy tính</td>
                    <td>
                        <a class="btn_mod" href="">Xóa</a>
                        <a class="btn_mod" href="">Sửa</a>
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Trần Thị B</td>
                    <td>Khoa học máy tính</td>
                    <td>
                        <a class="btn_mod" href="">Xóa</a>
                        <a class="btn_mod" href="">Sửa</a>
                    </td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Nguyễn Hoàng C</td>
                    <td>Khoa học vật liệu</td>
                    <td>
                        <a class="btn_mod" href="">Xóa</a>
                        <a class="btn_mod" href="">Sửa</a>
                    </td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Đinh Quang D</td>
                    <td>Khoa học vật liệu</td>
                    <td>
                        <a class="btn_mod" href="">Xóa</a>
                        <a class="btn_mod" href="">Sửa</a>
                    </td>
                </tr>
            </table>
        </div>           
            

        </fieldset>
    </body>


</html>